var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// dot controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active_dot", "");
  }
  slides[slideIndex-1].style.display = "block";
  var nodes = slides[slideIndex-1].childNodes;
  var width;
  for (var i = 0;i < nodes.length; i++) {
    var node = nodes[i];
    if (node.nodeType == 1) {
      if (node.nodeName == "IMG") {
        width = node.clientWidth;
      }
    }
  }
  width = String(width) + "px";
  slides[slideIndex - 1].parentElement.style.width = width;
  slides[slideIndex - 1].style.width = width;
  dots[slideIndex-1].className += " active_dot";
}
